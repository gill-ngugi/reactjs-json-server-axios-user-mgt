import axios from "axios";

const baseUrl = "http://localhost:5000/users";

export const getUsers = async (id) => {
    id = id || '';
    return await axios
        .get(`${baseUrl}/${id}`)
        .catch((e) => {
            console.log("Error", e);
        });
}

export const addUser = async (user) => {
    return await axios
        .post(baseUrl, user)
        .catch((e) => {
            console.log("Error", e);
        });
}

export const editUser = async (id, user) => {
    return await axios
        .put(`${baseUrl}/${id}`, user)
        .catch((e) => {
            console.log("Error", e);
        });
}

export const deleteUser = async (id) => {
    return await axios
        .delete(`${baseUrl}/${id}`)
        .catch((e) => {
            console.log("Error", e);
        })       
}