import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Header from "./components/Header";
import Home from "./components/Home";
import AllUsers from "./components/AllUsers";
import AddUser from "./components/AddUser";
import EditUser from "./components/EditUser";
import NotFound from "./components/NotFound";

function App() {
  return (
    <div>
      <Router>
        <Header />
        <Routes> 
          <Route path='*' element={<NotFound />} />
          <Route path="/" exact element={ <Home /> } />  
          <Route path="/users" exact element={ <AllUsers /> } />  
          <Route path="/add" exact element={ <AddUser /> } />  
          <Route path="/edit/:id" exact element={ <EditUser/> } />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
