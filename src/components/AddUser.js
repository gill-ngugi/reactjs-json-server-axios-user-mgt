import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { addUser, getUsers } from "../service/APIs";

const AddUser = () => {
    const styles = {
        container: {
            width: "60%",
            margin: "auto",
            textAlign: "center",
            paddingTop: "20px"
        },
        textField: {
            marginBottom: "15px"
        },
        span: {
            width:"5%"   
        }
    }

    const [formData, setFormData] = useState({
        name: "",
        username: "",
        email: "",
        phone: ""
    }); 

    const [users, setUsers] = useState([]);

    const getUsersData = async () => {
        const response = await getUsers();
        setUsers(response.data);
    }

    const navigate = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault();
        if(formData.name === "" || formData.username === "" || formData.email === "" || formData.phone === ""){
            alert("All fields are required")
        }
        else{
            console.log(formData);
            setFormData({ 
                name: "",
                username: "",
                email: "",
                phone: ""
            });
            navigate("/users");
            getUsersData();
        }
    }

    const addUserDetails = async () => {
        await addUser(formData);
    }

    const clearFormData = () => {
        setFormData({ 
            name: "",
            username: "",
            email: "",
            phone: ""
        });
    }

    return (
        <div style={styles.container}>
            <h4>Add User</h4>
            <form onSubmit={handleSubmit}>
                <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon1" style={styles.span}>@</span>
                    <input type="text" className="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1"
                        value={formData.name}
                        onChange={(e) => setFormData({...formData, name: e.target.value})} 
                    />
                </div> 
                <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon1" style={styles.span}>@</span>
                    <input type="text" className="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1"
                        value={formData.username}
                        onChange={(e) => setFormData({...formData, username: e.target.value})} 
                    />
                </div>  
                <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon1" style={styles.span}>@</span>
                    <input type="email" className="form-control" placeholder="Email" aria-label="Username" aria-describedby="basic-addon1"
                        value={formData.email}
                        onChange={(e) => setFormData({...formData, email: e.target.value})} 
                    />
                </div>  
                <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon1" style={styles.span}>@</span>
                    <input type="number" className="form-control" placeholder="Phone" aria-label="Username" aria-describedby="basic-addon1"
                        value={formData.phone}
                        onChange={(e) => setFormData({...formData, phone: e.target.value})} 
                    />
                </div> 
                <div>
                    <input className="btn btn-primary" type="reset" value="Clear" style={{marginRight: "30px"}} 
                        onClick = {(e) => clearFormData()} />
                    <input className="btn btn-primary" type="submit" value="Add User" 
                        onClick={(e) => (addUserDetails())} />
                </div>
            </form>
        </div>
    );
}

export default AddUser;