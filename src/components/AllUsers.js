import { Table, TableHead, TableRow, TableBody, TableCell, Button, Pagination } from "@mui/material";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { getUsers, deleteUser } from "../service/APIs";

const AllUsers = () => {

    const styles = 
    {
        container: {
            margin: "5%",
            marginTop: "2%"
        },
        thead2: {
            backgroundColor: "#0074D9",
            color: "#FFF",
            fontSize: "19px"
        },
        tcell2: {
            fontWeight: "bold",
            color: "#FFF",
            fontSize: "19px"
        }
    };
    
    const [users, setUsers] = useState([]);

    const getAllUsers = async () => {
        const response = await getUsers();
        console.log(response.data);
        setUsers(response.data);
    }

    const deleteUserData = async (id) => {
        await deleteUser(id);
        getAllUsers();
    }

    useEffect(() => {
        getAllUsers();
    }, []);

    return (
        <div style={styles.container}>
            <h2>All Users</h2>
            <Table>
                <TableHead style={styles.thead2}> 
                    <TableRow >
                        <TableCell style={styles.tcell2}>Id</TableCell>
                        <TableCell style={styles.tcell2}>Name</TableCell>
                        <TableCell style={styles.tcell2}>Email</TableCell>
                        <TableCell style={styles.tcell2}>Username</TableCell>
                        <TableCell style={styles.tcell2}>Phone</TableCell>
                        <TableCell style={styles.tcell2}></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    { users.map((user) => (
                        <TableRow key={user.id}>
                            <TableCell style={{fontSize: "19px"}}>{user.id}</TableCell>
                            <TableCell style={{fontSize: "19px"}}>{user.name}</TableCell>
                            <TableCell style={{fontSize: "19px"}}>{user.email}</TableCell>
                            <TableCell style={{fontSize: "19px"}}>{user.username}</TableCell>
                            <TableCell style={{fontSize: "19px"}}>{user.phone}</TableCell>
                            <TableCell> 
                                <button type="button" class="btn btn-primary" style={{width: "100px", marginRight: "10px"}}
                                    component={Link}
                                    to={`/edit/${user.id}`}
                                >Edit</button>
                               
                                <Button variant="contained" color="error" style={{width: "100px"}}
                                    onClick={() => (deleteUserData(user.id))}>
                                Delete</Button>
                            </TableCell> 
                        </TableRow>
                    ))
                    }
                </TableBody>
            </Table>    
            {/* <div style={{width: "50%", margin: "auto", backgroundColor: "#d4d4d4"}}>
                <Pagination count={10} variant="outlined" shape="rounded" style={{marginTop: "40px"}} />
            </div> */}
        </div>
    );
}

export default AllUsers;