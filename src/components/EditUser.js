import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { getUsers, editUser } from "../service/APIs";

const EditUser = () => {
    const styles = {
        container: {
            width: "60%",
            margin: "auto",
            textAlign: "center",
            paddingTop: "20px"
        },
        textField: {
            marginBottom: "15px"
        },
        span: {
            width:"5%"   
        }
    }

    const [user, setUser] = useState({
        name: "",
        username: "",
        email: "",
        phone: ""
    }); 

    const { name, username, email, phone } = user;
    const navigate = useNavigate();
    const params = useParams();
    const { id } = params;

    const loadUserData = async () => {
        const response = await getUsers(id);
        setUser(response.data);
    }

    useEffect(() => {
        loadUserData();
    }, []);

    const [users, setUsers] = useState([]);

    const getAllUsers = async () => {
        const response = await getUsers();
        setUsers(response.data);
    }

    const editUserDetails = async () => {
        await editUser(id, user);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if(name === "" || username === "" || email === "" || phone === ""){
            alert("All fields are required")
        }
        else{
            console.log(user);
            setUser({ 
                name: "",
                username: "",
                email: "",
                phone: ""
            });
            navigate("/users");
            getAllUsers();
        }
    }

    return (
        <div style={styles.container}>
            <h4>Edit User</h4>
            <form onSubmit={handleSubmit}>
                <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon1" style={styles.span}>@</span>
                    <input type="text" className="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1"
                        value={name}
                        onChange={(e) => setUser({...user, name: e.target.value})} 
                    />
                </div> 
                <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon1" style={styles.span}>@</span>
                    <input type="text" className="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1"
                        value={username}
                        onChange={(e) => setUser({...user, username: e.target.value})} 
                    />
                </div>  
                <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon1" style={styles.span}>@</span>
                    <input type="email" className="form-control" placeholder="Email" aria-label="Username" aria-describedby="basic-addon1"
                        value={email}
                        onChange={(e) => setUser({...user, email: e.target.value})} 
                    />
                </div>  
                <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon1" style={styles.span}>@</span>
                    <input type="number" className="form-control" placeholder="Phone" aria-label="Username" aria-describedby="basic-addon1"
                        value={phone}
                        onChange={(e) => setUser({...user, phone: e.target.value})} 
                    />
                </div> 
                <div>
                    <input className="btn btn-primary" type="submit" value="Edit User" 
                        onClick={(e) => (editUserDetails())} />
                </div>
            </form>
        </div>
    );
}

export default EditUser;