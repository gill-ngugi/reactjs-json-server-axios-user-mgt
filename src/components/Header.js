import { AppBar, Toolbar, Button, IconButton, Box } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import { NavLink } from 'react-router-dom';

const Header = () => {

    const navLinkStyle = {
        marginRight: "15px",
        textDecoration: "none",
        fontSize: "25px",
        color: "white"
    }

    return (
    <div>
        <Box sx={{ flexGrow: 1 }}>
            <AppBar style={{position:"static", backgroundColor: "#0074D9"}}>
                <Toolbar>
                    <IconButton
                        size="large"
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2 }}
                    >
                        <MenuIcon />
                    </IconButton>
                    {/* <NavLink to="/" style={navLinkStyle} >Home</NavLink> */}
                    <NavLink to="/users" style={navLinkStyle} >All Users</NavLink>
                    <NavLink to="/add" style={navLinkStyle} >Add User</NavLink>
                    <div style={{ flexGrow: 1 }}>
                    </div>
                    <Button color="inherit" >Login</Button>
                </Toolbar>
            </AppBar>
        </Box>
    </div>
    );
}

export default Header;