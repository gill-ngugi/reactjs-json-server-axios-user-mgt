*** STEPS ***
1. npx create-react-app@5.0.0 my-app
2. npm install @mui/material @emotion/react @emotion/styled
3. npm install @mui/styles
4. npm i react-router-dom
5. Npm install -g json-server … install globally
6. Npm install json-server … locally
7. Under package.json -> “scripts” -> add -> “server”: “json-server —watch db.json —port 5000” or “json-server —watch src/Database/db.json”
8. Terminal: npm run server 
9. Npm install concurrently
10. Under package.json -> “scripts” -> add -> "dev": "concurrently \"npm start\" \"npm run server\" " 
11. Terminal: npm run dev
12. Npm install axios
13. Resource => https://www.youtube.com/watch?v=A73N9tREM_Q ... code for interview
14. Article (useState -> Forms) => https://dev.to/jleewebdev/using-the-usestate-hook-and-working-with-forms-in-react-js-m6b

*** ADD ONS ***
Npm I @material-ui/core
Bootstrap CDN

*** REACT HOOKS ***
The useDispatch hook is used to dispatch an action while useSelector hook is used to get the state from the redux store
OR
useDispatch is used to modify values of a state while the useSelector hook is used to access values of the state.

useEffect is called whenever a component is mounted or when it’s updated.
1. Call useEffect whenever a state in your component gets changed. In the example below, useEffect will be called whenever either time or message states are changed. Hence the console log will be logged in both updates.
	const [time, setTime] = useState(“”);
	const [message, setMessage] = useState(“”);

	useEffect(() => {
		console.log(“Log Log Log”);
	});

2. Call useEffect only when the component is mounted and not when the states are updated. In the example below, useEffect will only be called the first time the component is mounted and not when any of the states change. After the log is output the first time during component mounting, it won’t happen again even if the states get updated.
	const [time, setTime] = useState(“”);
	const [message, setMessage] = useState(“”);

	useEffect(() => {
		console.log(“This will only log during mounting”);
	}, []);

3. Call useEffect only when one state changes and not when another state changes. In the example below, useEffect will only be called when the time state changes and not when the message state changes. This is because, the time variable is passed as a dependency in the array in useEffect hook.
	const [time, setTime] = useState(“”);
	const [message, setMessage] = useState(“”);

	useEffect(() => {
		console.log(“This will only log when the state of the time variable changes”);
	}, [time]);
